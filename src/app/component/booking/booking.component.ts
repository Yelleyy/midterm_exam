import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BookService } from 'src/app/share/book.service';
import { ProvinceService } from 'src/app/share/province.service';
import { Book } from './book';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css'],
})
export class BookingComponent implements OnInit {
  title = 'Yelley Airline';
  books: FormGroup;
  bookList !: Book[];
  provinceList: Province[] = [];
  datedep: any[] = []
  datearr: any[] = []
  count = 1;
  show: boolean = false;

  constructor(private provinceService: ProvinceService, private form: FormBuilder, private bookService: BookService) {
    this.books = this.form.group({
      fullName: ['', Validators.required],
      from: ['', Validators.required],
      to: ['', Validators.required],
      type: ['', Validators.required],
      departure: ['', Validators.required],
      arrival: [''],
      adults: [0, [Validators.required, Validators.pattern("[0-9]*$")]],
      children: [0, Validators.pattern("[0-9]*$")],
      infants: [0, Validators.pattern("[0-9]*$")],
    })

    //Province Service
    this.provinceService.getProvinceList().subscribe((response: any) => {
      this.provinceList = response;
    });

    //getBooklist
    this.bookList = this.bookService.getBooks();
  }
  //submit
  submit(book: Book) {
    Swal.fire({
      title: 'ยืนยันการจองตั๋ว ?',
      text: 'เมื่อสั่งซื้อแล้วไม่สามารถเปลี่ยนแปลงได้ !',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'สำเร็จ!',
          'กำลังดำเนินการจองตั๋ว...',
          'success'
        ).then((result2) => {
          if (result2.value) {
            this.show = true;
            //format date
            let resultarr = book.arrival.toLocaleDateString('th-TH', {
              year: 'numeric',
              month: 'long',
              day: 'numeric',
              weekday: 'long',
            })
            this.datearr.push(resultarr);
            let resultdep = book.departure.toLocaleDateString('th-TH', {
              year: 'numeric',
              month: 'long',
              day: 'numeric',
              weekday: 'long',
            })
            this.datedep.push(resultdep);
            this.show = true;
            if (this.count == 1) {
              this.clearBooks();
              this.count++;
            }
            this.bookService.newBooks(book);
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire({
          title: 'ยกเลิกแล้ว',
          text: 'ยกเลิกการจองแล้ว..',
          icon: 'error',
          confirmButtonText: 'ยืนยัน',
        })
      }
    })
    if ((book.adults || book.children || book.infants) == 0) {
      Swal.fire({
        title: 'ผิดพลาด!!',
        text: 'กรุณาใส่จำนวนที่นั่ง...',
        icon: 'error',
        confirmButtonText: 'ยืนยัน',
      })
      return false
    }
    if (book.from == book.to) {
      Swal.fire({
        title: 'ผิดพลาด!!',
        text: 'กรุณาเลือกสนามบินให้ถูก...',
        icon: 'error',
        confirmButtonText: 'ยืนยัน',
      })
      return false
    }
    return false
  }
  //delete
  delBooks(book: any) {
    this.bookList.forEach((element, index) => {
      if (element == book) {
        this.bookList.splice(index, 1);
      }
    });
  }
  clearBooks() {
    this.bookList.splice(0, 1);
  }

  ngOnInit(): void {
  }
}
interface Province {
  id: string;
  name_th: string;
  name_en: string;
  geography_id: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
}
