import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ProvinceService {

  constructor(private http:HttpClient) { }

  getProvinceList(){
    return  this.http.get("https://raw.githubusercontent.com/kongvut/thai-province-data/master/api_province.json");
  }

}
