import { Injectable } from '@angular/core';
import { Book } from '../component/booking/book';
import { Mockflight } from './mockflight';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  books: Book[]=[];

  constructor() {
    this.books = Mockflight.mockFlight;
  }

  getBooks = ()=> this.books;

  newBooks = (a:any) =>{
    this.books.push({
      fullName: a.fullName,
      from: a.from,
      to: a.to,
      type: a.type,
      departure: a.departure,
      arrival: a.arrival,
      adults: a.adults,
      children: a.children,
      infants: a.infants
    })
  }

}
