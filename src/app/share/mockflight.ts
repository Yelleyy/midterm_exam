import { Book } from "../component/booking/book";
export class Mockflight {
  public static mockFlight: Book[]=[{
    fullName: "Pattarapol Pornsirirung",
    from: "BKK",
    to: "CNX",
    type: "Return",
    departure: new Date("01-01-2565"),
    arrival: new Date("01-31-2565"),
    adults: 3,
    children: 0,
    infants: 0
  }]
}
